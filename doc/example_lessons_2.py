# RGB 0...255 ---> (255, 123, 34)
import random

while True:
    quantity = input("введіть кількість кольорів для генерації (ціле число): ")
    if not quantity:
        break
    if not quantity.isdigit():
        print("введіть, будь ласка, ціле число...")
    else:
        quantity = int(quantity)
        for number in range(quantity):
            R, G, B = random.randint(1, 255), random.randint(1, 255), random.randint(1, 255)
            print(f"RGB color is: R={R:>3}, G={G:>3}, B={B:>3}")
