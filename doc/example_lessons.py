GRAYSCALE = ('white', 'black', 'gray')
COLORFULLY = ('red', 'orange', 'yellow', 'green', 'cyan', 'blue', 'purple')


while True:
    color = input("input color: ")
    if not color:
        break
    if color not in GRAYSCALE + COLORFULLY:
        print("it is wrong color name, try...")
    else:
        print(f"it is {'grayscale' if color in GRAYSCALE else 'colorfully'}")
